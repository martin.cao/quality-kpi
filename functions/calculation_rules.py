"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""


def test_function():
    """Test function to check module functionality"""
    print("You called the test function NOO.")


def kpi_sum(*args):
    """
    Calculates the sum of one or more integers or lists.

    Args:
        *args: One or more integers or lists.

    Returns:
        total (int): The sum of all given integers and/or the sum
        of all items in all given lists.

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than int or list).
    """
    total = 0
    for arg in args:
        if isinstance(arg, int):  # Check if argument is an integer
            total += arg
        elif isinstance(arg, list):  # Check if argument is a list
            total += sum(arg)
        else:
            raise TypeError(
                f"Unsupported type {type(arg)} passed to kpi_sum()")
    return total

    # if arguments are handed over not as a list: sum(list(args))


# Add new functions for calculating some real complicated metrics


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )

def kpi_max(args):
    """
    KPI to return the max value of all arguments
    """
    return max(args)

def kpi_torque(torque, teeth):
    """
    KPI to calculate the Torque on the driving axles after gear transmissions
    """
    if len(teeth) < 2:
        raise TypeError(
                f"Need atleast 2 gears to calculate Transmission, number of teeth: {len(teeth)}")
        return None    
    else:
        rate = 0
        rate = teeth[0] / teeth[-1]
        Tq = torque*rate
        return Tq
    
def kpi_totalmass(masses):
    total_mass = 0
    for mass in masses:
        total_mass += mass
    return total_mass

def kpi_price(pricesarr):
    price = 0
    for prices in pricesarr:
        price += prices
    return price
        